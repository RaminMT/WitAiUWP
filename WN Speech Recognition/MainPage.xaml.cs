﻿/*
 * Downloaded from Win Nevis Community
 * Author: Ramtin Jokar
 * https://www.win-nevis.com
 * 
 */

using System;
using System.Collections.Generic;
using System.IO;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// using haye morede niaz
using Windows.Devices.Enumeration;
using Windows.Media.Audio;
using Windows.Media.Devices;
using Windows.Media.Render;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Windows.Storage;
using Windows.Media.MediaProperties;
using Windows.Media.Capture;
using Windows.Storage.Streams;

namespace WN_Speech_Recognition
{
    public sealed partial class MainPage : Page
    {
        AudioGraph graph;
        AudioFileOutputNode outputNode;
        StorageFile file;
        StorageFolder localFolder = ApplicationData.Current.LocalFolder;
        const string accessToken = "3PLCUPJISQTSUUAV3YLXFY3D3365MUTY";
        public MainPage()
        {
            this.InitializeComponent();
        }

        async private void btnRecordStop_Click(object sender, RoutedEventArgs e)
        {
            if (btnRecordStop.Label == "Record")
            {
                file = await localFolder.CreateFileAsync("wn.wav", CreationCollisionOption.GenerateUniqueName);
                if (file == null)
                {
                    Debug.WriteLine("file is null");
                    return;
                }

                var fName = file.Name;
                Debug.WriteLine(fName);
                btnRecordStop.Label = "Stop";
                btnRecordStop.Icon = new SymbolIcon(Symbol.Stop);
                var result = await AudioGraph.CreateAsync(new AudioGraphSettings(AudioRenderCategory.Speech));

                if (result.Status == AudioGraphCreationStatus.Success)
                {
                    graph = result.Graph;

                    var microphone = await DeviceInformation.CreateFromIdAsync(
                      MediaDevice.GetDefaultAudioCaptureId(AudioDeviceRole.Default));

                    // mono 16 bit, keyfiyate zabtesh paeine ama khobe
                    // wit ta 32 bishtar support nemikone pas 41 ya 48 bit ro nafrestid
                    var outProfile = MediaEncodingProfile.CreateWav(AudioEncodingQuality.Low);
                    outProfile.Audio = AudioEncodingProperties.CreatePcm(16000, 1, 16);

                    var outputResult = await graph.CreateFileOutputNodeAsync(file, outProfile);

                    if (outputResult.Status == AudioFileNodeCreationStatus.Success)
                    {
                        outputNode = outputResult.FileOutputNode;

                        var inProfile = MediaEncodingProfile.CreateWav(AudioEncodingQuality.High);

                        var inputResult = await graph.CreateDeviceInputNodeAsync(
                          MediaCategory.Speech,
                          inProfile.Audio,
                          microphone);

                        if (inputResult.Status == AudioDeviceNodeCreationStatus.Success)
                        {
                            inputResult.DeviceInputNode.AddOutgoingConnection(
                              outputNode);
                            // zabt seda shoro mishe
                            graph.Start();
                        }
                    }
                }
            }
            else
            {
                btnRecordStop.Label = "Record";
                btnRecordStop.Icon = new SymbolIcon(Symbol.Microphone);
                progressBar.Visibility = Visibility.Visible;
                if (graph != null)
                {
                    graph?.Stop();
                    await outputNode.FinalizeAsync();
                    graph?.Dispose();

                    graph = null;
                    var fName = file.Name;
                    Debug.WriteLine(fName);
                    file = null;
                    file = await localFolder.GetFileAsync(fName);
                    using (HttpClient client = new HttpClient())
                    {
                        //$ curl -XPOST 'https://api.wit.ai/speech?v=20160526' \
                        // -i -L \
                        // -H "Authorization: Bearer $TOKEN" \
                        // -H "Content-Type: audio/wav" \
                        // --data-binary "@sample.wav"
                        // addresse sende stream be wit
                        Uri targetUri = new Uri("https://api.wit.ai/speech?v=20160526");
                        // niaz hast ke chand ta header taein konim
                        Dictionary<string, string> headers = new Dictionary<string, string>();

                        // hamishe chunked bayad bashe
                        headers.Add("Transfer-encoding", "chunked");
                        // Client access token ro inja set mikonim
                        headers.Add("Authorization", "Bearer " + accessToken);

                        foreach (var item in headers)
                            client.DefaultRequestHeaders.Add(item.Key, item.Value);

                        // tuye header taein mikonim ke audio/wav ham support she
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("audio/wav"));

                        // streame file ro migirim
                        using (IRandomAccessStream fileStream = await file.OpenAsync(FileAccessMode.ReadWrite))
                        {
                            // chon mikhaim byte post konim be ByteArrayContent niaz darim
                            // stream ro midim behesh
                            using (ByteArrayContent byteContent = new ByteArrayContent(ReadStream(fileStream.AsStreamForWrite())))
                            {
                                // ContentType ro audio/wav mizarim
                                byteContent.Headers.ContentType = new MediaTypeHeaderValue("audio/wav");
                                // data ro post mikonim
                                HttpResponseMessage response = await client.PostAsync(targetUri, byteContent);

                                // matne jsoni ke response mide ro migirim
                                var content = new StreamReader(await response.Content.ReadAsStreamAsync()).ReadToEnd();

                                // Deserialize mikonim be WitResponseObject
                                var wit = JsonConvert.DeserializeObject<WitResponseObject>(content);
                                // matnesho tu textblockemon neshon midim
                                txtContent.Text = wit.Text;

                            }
                        }
                    }
                    try
                    {
                        await file.DeleteAsync();
                    }
                    catch (Exception ex) { Debug.WriteLine("ex while deleting file: " + ex.Message); }
                }
                progressBar.Visibility = Visibility.Collapsed;
            }
        }

        private void btnOpenWN_Click(object sender, RoutedEventArgs e)
        {
            OpenUrlInBrowser(new Uri("https://www.win-nevis.com"));
        }




        public static byte[] ReadStream(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        public async void OpenUrlInBrowser(Uri uri)
        {
            await Windows.System.Launcher.LaunchUriAsync(uri);
        }
    }
}
