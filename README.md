# WitUWP
# Wit Ai Sample
#### Windows Universal Platform (UWP) written by C# .
## Author: Ramtin Jokar

### Requirement
- This project uses Json.Net as Json parser.
- Capabilities: Microphone, Internet(Client), Internet(Client, Server)
- Minimum SDK: 10.0.10586

[Parse Dev Studio](https://www.parsedev.com) 

[Win Nevis Forum](https://www.win-nevis.com) 

### Wit Ai Official website
[Wit.Ai](https://wit.ai)

